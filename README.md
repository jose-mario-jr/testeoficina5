# Aplicação desenvolvida em 03 de fevereiro de 2020 como teste para admissão na Oficina5 Soluções Inteligentes

* backend em node.js utilizando arquitetura rest
* front-end utilizando javascript puro, e manipulação de DOM com template-strings, e requisições com xhr.
* testes da API feitos em INSOMNIA, exports se encontram na raiz!

# Para rodar o projeto, executar os comandos:

```
$ cd back
```
```
$ npm install
```
```
$ nodemon ./bin/server.js
```

 Após isso, abra front/index.html com o live server na porta 5500 (a origem deve ser http://127.0.0.1:5500), e está tudo pronto!
 
 Seria necessário colocar o ip na whitelist na conta do atlas para utilizar o banco de dados, por isso recomendo que utilize uma conexão própria e depois disso altere a connectionString em config.js