"use strict"

const express = require("express")
const router = express.Router()

const controller = require("../controllers/post-controller")

router.get("/", controller.get)
router.get("/:id", controller.getById)
router.get("/:id/comments", controller.getComments)
router.post("/:id/comments", controller.newComment)
router.post("/", controller.post)
router.put("/:id", controller.put)
// add patch
router.delete("/:id", controller.delete)

module.exports = router
