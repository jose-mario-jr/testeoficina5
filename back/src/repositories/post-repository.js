"use strict"

const mongoose = require("mongoose")
const Post = mongoose.model("Post")

exports.get = async () => {
  const res = await Post.find({})
  return res
}

exports.getById = async id => {
  const res = await Post.findById(id)
  return res
}

exports.getComments = async id => {
  const res = Post.findById(id, "comments")
  return res
}

exports.newComment = async (id, data) => {
  await Post.findByIdAndUpdate(id, {
    $push: {
      comments: {
        name: data.name,
        email: data.email,
        body: data.body
      }
    }
  })
}
exports.create = async data => {
  var product = new Post(data)
  await product.save()
}

exports.update = async (id, data) => {
  await Post.findByIdAndUpdate(id, {
    $set: {
      title: data.title,
      body: data.body
    }
  })
}

exports.delete = async id => {
  await Post.findByIdAndRemove(id)
}
