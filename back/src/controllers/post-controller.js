"use strict"

const ValidationContract = require("../validators/fluent")
const repository = require("../repositories/post-repository")

//const azure = require('azure-storage')

exports.get = async (req, res, next) => {
  try {
    var data = await repository.get()
    res.status(200).send(data)
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisição"
    })
  }
}

exports.getById = async (req, res, next) => {
  try {
    var data = await repository.getById(req.params.id)
    res.status(200).send(data)
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisição"
    })
  }
}

exports.getComments = async (req, res, next) => {
  try {
    const data = await repository.getComments(req.params.id)
    res.status(200).send(data)
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisição"
    })
  }
}

exports.post = async (req, res, next) => {
  let contract = new ValidationContract()
  contract.hasMinLen(
    req.body.title,
    3,
    "O titulo deve conter pelo menos 3 caracteres"
  )
  contract.hasMinLen(
    req.body.body,
    3,
    "O corpo deve conter pelo menos 3 caracteres"
  )

  //validar contrato
  if (!contract.isValid()) {
    res
      .status(400)
      .send(contract.errors())
      .end()
    return
  }

  try {
    await repository.create(req.body)
    res.status(201).send({ message: "Post cadastrado com sucesso!" })
  } catch (e) {
    res.status(400).send({
      message: "Falha ao cadastrar Post!",
      data: e
    })
  }
}

exports.newComment = async (req, res, next) => {
  try {
    await repository.newComment(req.params.id, req.body)
    res.status(201).send({ message: "Comentario criado com sucesso!" })
  } catch (e) {
    res.status(400).send({
      message: "Falha ao cadastrar commentario!",
      data: e
    })
  }
}

exports.put = async (req, res, next) => {
  try {
    await repository.update(req.params.id, req.body)
    res.status(200).send({
      message: "Post atualizado com sucesso!"
    })
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisição"
    })
  }
}

exports.delete = async (req, res, next) => {
  try {
    await repository.delete(req.params.id)
    res.status(200).send({ message: "Post removido com sucesso!" })
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisição"
    })
  }
}
